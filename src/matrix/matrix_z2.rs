
use std::fmt;

use crate::integer::Number;

#[derive(Debug)]
pub struct MatrixZ2 {
    rows: Vec<Box<Number>>
}


impl MatrixZ2 {
    pub fn new() -> MatrixZ2 {
        MatrixZ2 {
            rows: Vec::new()
        }
    }


    pub fn identity(n: usize) -> MatrixZ2 {
        let mut mat = MatrixZ2::new();
        let mut number = Number::from(1);

        for _ in 0..n {
            mat.append_row(&number);
            number.mul_digit(2);
        }

        mat
    }

    
    pub fn append_row(&mut self, row: &Number) -> () {
        self.rows.push(Box::new(row.clone()));
    }


    pub fn get_size(&self) -> (usize, usize) {
        let mut cols = 0;
        for row in self.rows.iter() {
            let curr_cols = row.top_bit_index();
            if curr_cols > cols {
                cols = curr_cols;
            }
        }
        (self.rows.len(), cols + 1)
    }


    pub fn get_row(&self, row: usize) -> &Number {
        if row >= self.rows.len() {
            panic!("Row index out of range!");
        }
        &self.rows[row]
    }


    pub fn row_reduce_augmented(&mut self) -> MatrixZ2 {
        let mut augmentation = MatrixZ2::identity(self.rows.len());

        let (_, mut bit) = self.get_size();
        bit += 1;
        'outer: for r in 0..self.rows.len() {
            let mut active_row_done = false;
            while !active_row_done {
                if bit == 0 {
                    break 'outer;
                }
                bit -= 1;

                for rr in r..self.rows.len() {
                    if self.rows[rr].is_bit_set(bit) {
                        self.rows.swap(r, rr);
                        augmentation.rows.swap(r, rr);

                        let tmp_self = *self.rows[r].clone();
                        let tmp_aug = *augmentation.rows[r].clone();
                        for rrr in (rr+1)..self.rows.len() {
                            if self.rows[rrr].is_bit_set(bit) {
                                self.rows[rrr].bitxor(&tmp_self);
                                augmentation.rows[rrr].bitxor(&tmp_aug);
                            }
                        }

                        active_row_done = true;
                        break;
                    }
                }
            }
        }
        augmentation
    }
}


impl fmt::Display for MatrixZ2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (_, cols) = self.get_size();

        for row in &self.rows {
            write!(f, "[ ")?;

            let val = format!("{:b}", **row);

            for _ in 0..(cols - val.len()) {
                write!(f, " ")?;
            }
            writeln!(f, "{} ]", val)?;
        }
        Result::Ok(())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_matrix() {
        let mut x = MatrixZ2::new();

        x.append_row(&Number::from(0b1000));
        x.append_row(&Number::from(0b0111));
        x.append_row(&Number::from(0b1111));


        let res = x.row_reduce_augmented();

        assert_eq!(*x.get_row(2), Number::from(0));
        assert_eq!(*res.get_row(2), Number::from(7));
    }


    #[test]
    fn test_matrix_large_random() {
        use std::time::Instant;
        
        let mut x = MatrixZ2::new();

        let size = 10000;
        for _ in 0..(size+1) {
            x.append_row(&Number::gen_random_n_bits(size));
        }
        
        //println!("{}", x);

        let now = Instant::now();
        let _res = x.row_reduce_augmented();
        let duration = now.elapsed();

        println!("Time for row reduction: {:?}", duration);
        //println!("{}", x);
        //println!("{}", _res);

        //println!("{:b}",  _res.get_row(size));
        assert_eq!(*x.get_row(size), Number::from(0));
    }


    #[test]
    fn test_matrix_large_sparse_random() {
        use std::time::Instant;
        
        let mut x = MatrixZ2::new();

        let size = 10000;
        for _ in 0..(size+1) {
            x.append_row(&Number::gen_random_sparse_n_bits(size, 15));
        }
        
        let now = Instant::now();
        let _res = x.row_reduce_augmented();
        let duration = now.elapsed();

        println!("Time for row reduction: {:?}", duration);

        assert_eq!(*x.get_row(size), Number::from(0));
    }


    #[test]
    fn test_matrix_large_sparse_random_with_unused_columns() {
        use std::time::Instant;
        use rand::Rng;

        let limit = 10000;
        let limit_75 = (limit * 3) / 4;
        let mut x = MatrixZ2::new();

        let mut rng = rand::thread_rng();
        let mut mask = Number::from(0);
        for _ in 0..limit_75 {
            mask.set_bit(rng.gen_range(0..limit));
        }

        for _ in 0..(limit+1) {
            let mut num = Number::gen_random_sparse_n_bits(limit, 15);
            num.bitand(&mask);
            x.append_row(&num);
        }
        
        let now = Instant::now();
        let _res = x.row_reduce_augmented();
        let duration = now.elapsed();

        println!("Time for row reduction: {:?}", duration);

        assert_eq!(*x.get_row(limit), Number::from(0));
    }
}
