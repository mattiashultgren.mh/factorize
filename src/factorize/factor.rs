
use std::io::{stdout, Write};
use std::num::NonZeroUsize;
use std::time::Instant;
use std::thread;

use crate::integer::Integer;
use crate::imath;
use crate::primes::prime_sieve;
use super::quadratic_sieve::{QSStats, QuadraticSieve};


#[derive(Debug)]
pub enum FactorResult {
    Prime,
    ProbablePrime,
    NotPrimeIsBelowTwo,
    NoFactorFound,
    CompositeFactor(Integer, QSStats)
}


pub fn factor(n: &Integer) -> FactorResult {
    let timer = Instant::now();

    let timer_action = Instant::now();
    let res = imath::is_prob_prime(n);
    println!("Primality test, time: {:?}", timer_action.elapsed());
    match res {
        imath::PrimeResult::Prime => return FactorResult::Prime,
        imath::PrimeResult::ProbablePrime => return FactorResult::ProbablePrime,
        imath::PrimeResult::NotPrimeIsBelowTwo => return FactorResult::NotPrimeIsBelowTwo,
        imath::PrimeResult::CompositeFactor(f) => return FactorResult::CompositeFactor(f, QSStats::new()),
        imath::PrimeResult::CompositeWitness(_) => ()
    }

    let timer_action = Instant::now();
    let res = trial_division(n);
    println!("Trial division, time: {:?}", timer_action.elapsed());
    match res {
        FactorResult::CompositeFactor(f, stats) => return FactorResult::CompositeFactor(f, stats),
        _ => ()
    }

    let timer_action = Instant::now();
    println!("\nQuadratic Sieve");
    let mut sieve = QuadraticSieve::new(n, 
        if n.top_bit_index() + 1 < 50 {
            NonZeroUsize::new(1).unwrap()
        } else {
            match thread::available_parallelism() {
                Ok(threads) => threads,
                _ => NonZeroUsize::new(16).unwrap()
            }
        }
    );

    let mut try_solve_when_above = 1.0;
    loop {
        let ratio = sieve.collect_relations();

        let rel = sieve.num_relations_found();
        print!("Time: {} of estimated {:.0}  ", timer_action.elapsed().as_secs(),
               timer_action.elapsed().as_secs_f64() * (1.05 * (sieve.size_of_factor_base() as f64)/ (rel as f64)) );
        print!("{:.1} relations/second   ", rel as f64 / timer_action.elapsed().as_secs_f64());
        stdout().flush().unwrap_or(());

        if ratio > try_solve_when_above {
            try_solve_when_above += 0.01;
            let res = sieve.solve();
        
            match res {
                FactorResult::CompositeFactor(f, stats) => {
                    println!("");
                    println!("Quadratic sieve, stats: {:#?}", stats);
                    println!("Quadratic sieve, time: {:?}\n", timer_action.elapsed());
                    println!("Total time elapsed: {:?}", timer.elapsed());
                    return FactorResult::CompositeFactor(f, stats)
                },
                _ => ()
            }
        }
    }
}


fn trial_division(n: &Integer) -> FactorResult {
    let sqrt = n.isqrt();

    let upper_limit = Integer::from_string("10_000_000").unwrap();
    let limit = if sqrt < upper_limit { sqrt } else { upper_limit }.to_string().parse::<u64>().unwrap();
    let primes = prime_sieve(limit);

    for p in primes {
        let rest = n.remainder_digit(p);

        if rest == 0 {
            return FactorResult::CompositeFactor(Integer::from(p), QSStats::new())
        }
    }

    FactorResult::NoFactorFound
}
