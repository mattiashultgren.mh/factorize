
use std::time::Duration;


#[derive(Clone, Debug)]
pub struct QSStats {
    pub time_matrix_filtering: Duration,
    pub count_relations_before_filter: usize,
    pub count_relations_after_filter: usize,

    pub time_matrix_solving: Duration,
    pub counter_matrix_solving: u64,

    pub time_convert_to_factor: Duration,
    pub counter_convert_to_factor: u64,

    pub counter_relations_from_large_prime: u64,
}


impl QSStats {
    pub fn new() -> QSStats {
        QSStats {
            time_matrix_filtering: Duration::from_secs(0),
            count_relations_before_filter: 0,
            count_relations_after_filter: 0,
            time_matrix_solving: Duration::from_secs(0),
            counter_matrix_solving: 0,
            time_convert_to_factor: Duration::from_secs(0),
            counter_convert_to_factor: 0,
            counter_relations_from_large_prime: 0,
        }
    }
}

