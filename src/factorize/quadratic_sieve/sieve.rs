
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt;
use std::num::NonZeroUsize;
use std::sync::{Arc, Mutex, mpsc};
use std::thread;
use std::time::{Duration, Instant};

use crate::factorize::FactorResult;
use crate::imath::{self, is_prob_prime, PrimeResult};
use crate::integer::{Digit, Integer, Number};
use crate::matrix::MatrixZ2;
use crate::primes::prime_sieve;
use super::{NumberRelation, QSStats, Worker};


pub struct QuadraticSieve {
    relations: Vec<NumberRelation>,
    relations_with_large_prime: HashMap<Digit, NumberRelation>,
    factor_base: Arc<Vec<Digit>>,
    n: Integer,
    mask_factor_base_used: Number,
    threading_queue: mpsc::Receiver<NumberRelation>,
    wait_mutex: Arc<Mutex<()>>,

    solve_stats: QSStats,
}


impl fmt::Display for NumberRelation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {} ", self.x)?;
        write!(f, "factored has {} set bits", self.y2_in_factor_base.popcnt())
    }
}


impl QuadraticSieve {
    pub fn new(n: &Integer, workers: NonZeroUsize) -> QuadraticSieve {
        let factor_base_limit = QuadraticSieve::compute_factor_limit(n);
        let factor_base = prime_sieve(factor_base_limit);

        let poly_bound = QuadraticSieve::compute_polynom_sieve_bound(&n);
        println!(" Factor base has {} primes with the limit {} and polynom sieving bound {}", factor_base.len(), factor_base_limit, poly_bound);

        let mut thread_step = workers.get() + 1;
        for p in factor_base.iter() {
            if *p as usize >= workers.get() {
                thread_step = *p as usize;
                break;
            }
        }

        let factor_base = Arc::new(factor_base);

        let (tx, rx) = mpsc::channel();
        let mutex = Arc::new(Mutex::new(()));
        for i in 0..workers.get() {
            let tx_clone = tx.clone();
            let n_clone = n.clone();
            let factor_base_clone = Arc::clone(&factor_base);
            let wait_mutex_clone = Arc::clone(&mutex);
            thread::spawn(move || {
                let worker = Worker::new(
                    tx_clone,
                    wait_mutex_clone,
                    i,
                    thread_step,
                    n_clone,
                    factor_base_clone,
                    factor_base_limit,
                    poly_bound
                );
                worker.find_relations();
            });
        }

        QuadraticSieve {
            relations: Vec::new(),
            relations_with_large_prime: HashMap::new(),
            factor_base: factor_base,
            n: n.clone(),
            mask_factor_base_used: Number::from(0),
            threading_queue: rx,
            wait_mutex: mutex,
            solve_stats: QSStats::new(),
        }
    }


    fn compute_factor_limit(n: &Integer) -> u64 {
        // Bound formula from https://medium.com/nerd-for-tech/heres-how-quadratic-sieve-factorization-works-1c878bc94f81
        let approx_log2_n = (n.top_bit_index() as f64) + 1.0;
        let ln_n = approx_log2_n * std::f64::consts::LN_2;
        let bound = (0.485 * (ln_n * ln_n.ln()).sqrt()).exp() * 0.92;

        // Limit of 2M roughly gives 150000 primes as max, which would currently need ~3.5GB for the matrix solving phase
        let upper_limit = 2000000.0;

        let mut bound = Integer::from(if bound < upper_limit {bound} else {upper_limit} as u64);

        if bound.is_even() {
            bound.increment(1);
        }

        loop {
            bound.increment(2);
            match is_prob_prime(&bound) {
                PrimeResult::Prime => break,
                PrimeResult::ProbablePrime=> break,
                _ => (),
            }
        }

        bound.decrement(1);
        return bound.to_digit().unwrap();
    }


    fn compute_polynom_sieve_bound(n: &Integer) -> u64 {
        let nbits = n.top_bit_index() as u64 + 1;
        let mut bound = nbits * nbits * nbits / 9;

        if nbits < 64 {
            let rough_sqrt = 1 << ((nbits-1) / 2);

            let limit = rough_sqrt / 10;

            if bound > limit {
                bound = limit;
            }
        }

        let upper_limit = 32000000;
        bound = std::cmp::min(bound, upper_limit);

        return if bound < 4 { 4 } else { bound };
    }


    pub fn num_relations_found(&self) -> usize {
        self.relations.len()
    }


    pub fn size_of_factor_base(&self) -> usize {
        self.factor_base.len()
    }


    fn filter_relations(&self, rel_index: &mut Vec<usize>) {
        for _passes in 1..=4 {
            let size = rel_index.len();

            let mut mask_used = Number::from(0);
            let mut mask_used_twice_or_more = Number::from(0);

            for idx in rel_index.iter() {
                let mut mask = self.relations[*idx].y2_in_factor_base.clone();
                mask.bitand(&mask_used);
                mask_used_twice_or_more.bitor(&mask);

                mask_used.bitor(&self.relations[*idx].y2_in_factor_base);
            }

            let mut used_only_once = mask_used;
            used_only_once.bitxor(&mask_used_twice_or_more);
            
            // Remove all relations that have primes that is only used once
            for idx in (0..rel_index.len()).rev() {
                if !used_only_once.bitand_is_zero(&self.relations[rel_index[idx]].y2_in_factor_base) {
                    rel_index.swap_remove(idx);
                }
            }

            if size == rel_index.len() {
                break;
            }
        }
    }


    pub fn solve(&mut self) -> FactorResult {
        self.compute_and_print_ratio();

        let _tmp = self.wait_mutex.lock().unwrap();

        let mut matrix = MatrixZ2::new();

        // 160bit =>  90ms
        // 180bit =>  460ms
        // 200bit => 7600ms
        let timer = Instant::now();
        let mut rel_index = Vec::with_capacity(self.relations.len());
        self.solve_stats.count_relations_before_filter = self.relations.len();
        for idx in 0..self.relations.len() {
            rel_index.push(idx);
        }
        self.filter_relations(&mut rel_index);
        self.solve_stats.count_relations_after_filter = rel_index.len();
        self.solve_stats.time_matrix_filtering += timer.elapsed();

        // Build the matrix to find the dependency in
        for idx in rel_index.iter() {
            matrix.append_row(&self.relations[*idx].y2_in_factor_base);
        }

        let timer = Instant::now();
        let solution = matrix.row_reduce_augmented();
        self.solve_stats.time_matrix_solving += timer.elapsed();
        self.solve_stats.counter_matrix_solving += 1;

        for ri in (0..rel_index.len()).rev() {
            if matrix.get_row(ri).is_zero() {
                let timer = Instant::now();
                let factor = self.convert_to_factor(solution.get_row(ri), &rel_index);
                self.solve_stats.time_convert_to_factor += timer.elapsed();
                self.solve_stats.counter_convert_to_factor += 1;

                if let Some(f) = factor {
                    return FactorResult::CompositeFactor(f, self.solve_stats.clone());
                }
            } else {
                break;
            }
        }

        FactorResult::NoFactorFound
    }


    fn convert_to_factor(&self, solution: &Number, rel_index: &Vec<usize>) -> Option<Integer> {
        let mut x = Integer::from(1);
        let mut y2 = Integer::from(1);
        let mut y_part = Integer::from(1);
        let mut y_factor = Number::from(0);

        for idx in 0..(solution.top_bit_index() + 1) {
            if solution.is_bit_set(idx) {
                x = (&x * &self.relations[rel_index[idx]].x).remainder(&self.n);
                y2 = &y2 * &self.relations[rel_index[idx]].y2;

                // Now try to extract the sqrt of y2 by collecting half of all the primes that was noted down
                // This will not reach all the way, but it will reduce the size big time for the isqrt below
                for i in 0..(self.relations[rel_index[idx]].y2_in_factor_base.top_bit_index()+1) {
                    if self.relations[rel_index[idx]].y2_in_factor_base.is_bit_set(i) {
                        if y_factor.is_bit_set(i) {
                            y_factor.clear_bit_no_truncate(i);
                        } else {
                            y_part.mul(self.factor_base[i]);
                            y_factor.set_bit(i);
                        }
                    }
                }
            }
        }

        // Dividing out the know square part and let the isqrt finish up the job
        let reduced_y2 = &y2 / &(&y_part * &y_part);
//        let timer = Instant::now();
        let y = &reduced_y2.isqrt() * &y_part;
//        println!("isqrt( {} bits ) in {:?}", reduced_y2.top_bit_index() + 1, timer.elapsed());
        assert_eq!(&y * &y, y2, "y_part: {} y: {} y2: {}", y_part, y, y2);

        let y = y.remainder(&self.n);

        // Checking if x = ±y (mod n)
        if x == y  ||  x == ((&self.n - &y).remainder(&self.n)) {
            //println!("x = ±y (mod n), thus no solution!");
            return None
        }
        let f = imath::gcd(&(&x + &y), &self.n);

        if f.cmp(1) == Ordering::Equal  ||  f == self.n {
            panic!("  Failed (n: {}, x: {}, y: {}, f:{})", self.n, x, y, f);
        }
        return Some(f)
    }


    fn convert_to_factor_gcd_style(&self, solution: &Number) -> Option<Integer> {
        let mut x = Integer::from(1);
        let mut r = Integer::from(1);
        let mut q_bar = Integer::from(1);

        for idx in 0..(solution.top_bit_index() + 1) {
            if solution.is_bit_set(idx) {
                x = (&x * &self.relations[idx].x).remainder(&self.n);

                let tmp = imath::gcd(&r, &self.relations[idx].y2);
                q_bar = &tmp * &q_bar;

                //r = &(&r / &tmp) * &(&self.relations[idx].y2 / &tmp);
                r = &(&r * &self.relations[idx].y2) / &(&tmp * &tmp);
            }
        }

        // Dividing out the know square part and let the isqrt finish up the job
        //let timer = Instant::now();
        let tmp = r.isqrt();
        //println!("isqrt( {} bits ) in {:?}", r.top_bit_index() + 1, timer.elapsed());
        assert_eq!(&tmp * &tmp, r, "tmp: {} r: {}", tmp, r);

        let y = (&tmp * &q_bar).remainder(&self.n);

        // Checking if x = ±y (mod n)
        if x == y  ||  x == ((&self.n - &y).remainder(&self.n)) {
            //println!("x = ±y (mod n), thus no solution!");
            return None
        }
        let f = imath::gcd(&(&x + &y), &self.n);

        if f.cmp(1) == Ordering::Equal  ||  f == self.n {
            panic!("  Failed (n: {}, x: {}, y: {}, f:{})", self.n, x, y, f);
        }
        return Some(f)
    }


    fn add_relation(&mut self, r: NumberRelation) -> () {
        if let Some(large_prime) = r.large_prime {
            if let Some(relation) = self.relations_with_large_prime.remove(&large_prime) {
                let mut y2_in_factor_base = r.y2_in_factor_base.clone();
                y2_in_factor_base.bitxor(&relation.y2_in_factor_base);

                let large_prime = Integer::from(large_prime);
                if let Some(large_prime_inverse) = imath::mod_inverse(&large_prime, &self.n) {
                    self.add_relation(NumberRelation {
                        x: (&(&r.x * &relation.x) * &large_prime_inverse).remainder(&self.n),
                        y2: &(&r.y2 * &relation.y2) / &(&large_prime * &large_prime),
                        y2_in_factor_base: y2_in_factor_base,
                        large_prime: None
                    });
                } else {
                    self.add_relation(NumberRelation {
                        x: &r.x * &relation.x,
                        y2: &r.y2 * &relation.y2,
                        y2_in_factor_base: y2_in_factor_base,
                        large_prime: None
                    });
                }

                //println!("Merging {:?} and {:?}", r, relation);
                self.solve_stats.counter_relations_from_large_prime += 1;
            } else {
                //println!("Adding {:?}", r);
                self.relations_with_large_prime.insert(large_prime, r);
            }
        } else {
            self.mask_factor_base_used.bitor(&r.y2_in_factor_base);
            self.relations.push(r);
        }
    }


    pub fn collect_relations(&mut self) -> f64 {
        let timer = Instant::now();
        for _ in 0..(((self.factor_base.len() as f32).log2().powi(2)) as usize) {
            match self.threading_queue.recv() {
                Ok(relation) => self.add_relation(relation),
                _ => thread::sleep(Duration::from_millis(1)),
            }
            if timer.elapsed().as_secs() >= 1 {
                break;
            }
        }

        self.compute_and_print_ratio()
    }


    fn compute_and_print_ratio(&self) -> f64 {
        let primes_used = self.mask_factor_base_used.popcnt();
        let ratio = (self.relations.len() as f64) / (primes_used as f64);
        print!("\r  Relations {} ({}, {}) over {} primes at ratio {:.2}  ",
               self.relations.len(),
               self.solve_stats.counter_relations_from_large_prime, self.relations_with_large_prime.len(),
               primes_used, ratio);
        
        ratio
    }
}
