
use crate::integer::{Digit, Integer, Number};

#[derive(Debug, Clone)]
pub struct NumberRelation {
    // Stores one relation for eventually finding: x^2 = y^2 (mod n)
    pub x: Integer,
    pub y2: Integer,
    pub y2_in_factor_base: Number,
    // The y_in_factor_base should be interpreted as every bit in the Number is the exponent
    // of the factorizaion of y over the factor base where the exponents are saved mod 2,
    // i.e. only the eveness is stored in the Number.
    pub large_prime: Option<Digit>,
}
