

use std::cmp::Ordering;
use std::iter::Inspect;
use std::sync::{Arc, Mutex, mpsc};
use std::time::Instant;

use crate::imath::{self, sqrt_mod_p_pow_e};
use crate::integer::{Digit, Integer, Number};
use super::NumberRelation;


pub struct Worker {
    queue: mpsc::Sender<NumberRelation>,
    wait_mutex: Arc<Mutex<()>>,
    thread_index: usize,
    thread_step: usize,
    n: Integer,
    factor_base: Arc<Vec<Digit>>,
    factor_base_limit: Digit,
    poly_bound: Digit
}


impl Worker {
    pub fn new(
        queue: mpsc::Sender<NumberRelation>,
        wait_mutex: Arc<Mutex<()>>,
        thread_index: usize,
        thread_step: usize,
        n: Integer,
        factor_base: Arc<Vec<Digit>>,
        factor_base_limit: Digit,
        poly_bound: Digit
     ) -> Worker {
        Worker {
            queue: queue,
            wait_mutex: wait_mutex,
            thread_index: thread_index,
            thread_step: thread_step,
            n: n,
            factor_base: factor_base,
            factor_base_limit: factor_base_limit,
            poly_bound: poly_bound
        }
    }


    pub fn find_relations(&self) -> () {
        let mut poly_constant = Integer::from(self.thread_index as Digit + 1);

        let mut sieve_y2_log2: Vec<i16> = Vec::new();
        sieve_y2_log2.resize(self.poly_bound as usize, 0);

        loop {
            let (n, mut x) = self.fill_sieve(&poly_constant, &mut sieve_y2_log2);

            match self.wait_mutex.lock() {
                Ok(_) => (),
                Err(_) => panic!("Broken mutex!"),
            }

            match self.extract_relations_from_sieve(&n, &sieve_y2_log2, &mut x) {
                Ok(_) => (),
                Err(_) => return,
            }

            self.update_constant(&mut poly_constant);
        }
    }


    fn fill_sieve(&self, poly_constant: &Integer, sieve_y2_log2: &mut Vec<i16>) -> (Integer, Integer) {
//        let timer = Instant::now();

        let n = &self.n * poly_constant;
        let mut x = n.isqrt();
        x.increment(1);

        sieve_y2_log2.iter_mut().for_each(|m| *m = 0);

        for p in self.factor_base.iter() {
            if *p == 2 {
                Worker::fill_sieve_using_2(sieve_y2_log2, &x, &n);
            } else {
                let digit_n = n.remainder_digit(*p);
                if let Some(root) = imath::sqrt_mod_p(digit_n, *p) {
                    let tmp = x.remainder_digit(*p);
                    let idx = (*p + root - tmp) % *p;

                    let log2_p = (*p as f32).log2() as i16;

                    for i in ((idx as usize)..sieve_y2_log2.len()).step_by(*p as usize) {
                        sieve_y2_log2[i] += log2_p;
                    }
    
                    if root != 0 {
                        let root = *p - root;
                        let idx = (*p + root - tmp) % *p;
        
                        for i in ((idx as usize)..sieve_y2_log2.len()).step_by(*p as usize) {
                            sieve_y2_log2[i] += log2_p;
                        }

                        let mut p_pow_e = p*p;
                        for e in 2..=3 {
                            if p_pow_e > self.factor_base_limit {
                                break;
                            }

                            let n_mod = n.remainder_digit(p_pow_e);
                            let root_pow = sqrt_mod_p_pow_e(n_mod, *p, e, root);

                            if root_pow == 0 {
                                break;
                            }

                            let idx = (p_pow_e + root_pow - x.remainder_digit(p_pow_e)) % p_pow_e;
                            for i in ((idx as usize)..sieve_y2_log2.len()).step_by(p_pow_e as usize) {
                                sieve_y2_log2[i] += log2_p;
                            }

                            let idx = (idx + 2 * (p_pow_e - root_pow)) % p_pow_e;
                            for i in ((idx as usize)..sieve_y2_log2.len()).step_by(p_pow_e as usize) {
                                sieve_y2_log2[i] += log2_p;
                            }

                            p_pow_e *= p;
                        }
                    }
                }
            }
        }
//        println!("Fill sieve: {:?}", timer.elapsed());
        return (n, x);
    }


    fn extract_relations_from_sieve(&self, n: &Integer, sieve_y2_log2: &Vec<i16>, x: &mut Integer)
        -> Result<(), mpsc::SendError<NumberRelation>>
    {
//        let timer = Instant::now();
//        let mut extracted_relations = 0;

        let large_prime_extra_bound = std::cmp::min(8, ((self.factor_base_limit as f32).log2()) as i16);
        let mut sieve_rough_limit = x.top_bit_index() as i16 + 1; // floor(log2(2x))
        let sieve_limit = (self.factor_base_limit as f32).log2() as i16 - 1;
        
        sieve_rough_limit -= sieve_limit;
        
        let mut double_at = 2;
        let mut last_x_idx = 0;
        for idx in 0..sieve_y2_log2.len() {
            if idx == 0  ||  sieve_y2_log2[idx] >= sieve_rough_limit - large_prime_extra_bound {
                x.increment((idx - last_x_idx) as Digit);
                last_x_idx = idx;
                let y2 = (&*x * &*x).remainder(n);
                let y2_log2 = y2.top_bit_index() as i16; // floor(log2(y2))

                if (y2_log2 - sieve_y2_log2[idx]) < sieve_limit + large_prime_extra_bound {
                    if let Some((y2_in_factor_base, large_prime)) = self.factor_y2(&y2) {
                        let rel = NumberRelation {
                            x: x.clone(),
                            y2: y2,
                            y2_in_factor_base: y2_in_factor_base,
                            large_prime: if large_prime == 1 { None } else { Some(large_prime) },
                        };
                        self.queue.send(rel)?;
//                        extracted_relations += 1;
                    } else {
                        println!("Failed to factor the marked number! {} {} {}", sieve_limit, sieve_y2_log2[idx],
                                 y2
                        );
                    }
                }
            }

            if idx >= double_at {
                double_at *= 2;
                sieve_rough_limit += 1
            }
        }
//        println!("Extract {} relations from sieve: {:?}", extracted_relations, timer.elapsed());
        Ok(())
    }


    fn update_constant(&self, poly_constant: &mut Integer) -> () {
        loop {
            poly_constant.increment(self.thread_step as Digit);
            while poly_constant.is_doubly_even()  ||
                poly_constant.remainder_digit(3*3) == 0  ||
                poly_constant.remainder_digit(5*5) == 0  ||
                poly_constant.remainder_digit(7*7) == 0  ||
                poly_constant.remainder_digit(11*11) == 0  ||
                poly_constant.remainder_digit(13*13) == 0  ||
                poly_constant.remainder_digit(17*17) == 0
            {
                poly_constant.increment(self.thread_step as Digit);
            }

            let limit = 4;
            let mut num_hits = 0;
            for p in vec![3, 5, 7, 11, 13] {
                if imath::is_quadratic_residue_digit((&self.n * &poly_constant).remainder_digit(p), p) {
                    num_hits += 1;
                    if num_hits >= limit {
                        break;
                    }
                }
            }
            if num_hits >= limit {
                break;
            }
        }
    }


    fn fill_sieve_using_2(sieve_y2_log2: &mut Vec<i16>, x_base: &Integer, n: &Integer) -> () {
        let mut y2 = (x_base * x_base).remainder(&n);
        let idx = if y2.is_even() { 0 } else { 1 };

        for i in ((idx as usize)..sieve_y2_log2.len()).step_by(2) {
            sieve_y2_log2[i] += 1; // log2(p) = log2(2) = 1
        }

        let mut x_tmp = x_base.clone();
        if idx != 0 {
            x_tmp.increment(idx);
            y2 = (&x_tmp * &x_tmp).remainder(&n);
        }
        if y2.is_doubly_even() {
            for i in ((idx as usize)..sieve_y2_log2.len()).step_by(2*2) {
                sieve_y2_log2[i] += 1; // log2(p) = log2(2) = 1
            }
        } else {
            x_tmp.increment(2);    
            let y2 = (&x_tmp * &x_tmp).remainder(&n);
            if y2.is_doubly_even() {
                for i in (((idx + 2) as usize)..sieve_y2_log2.len()).step_by(2*2) {
                    sieve_y2_log2[i] += 1; // log2(p) = log2(2) = 1
                }
            }
        }
    }


    pub fn factor_y2(&self, y2: &Integer) -> Option<(Number, Digit)> {
        let mut y2 = y2.clone();
        let mut y2_in_factor_base = Number::from(0);

        for pi in 0..self.factor_base.len() {
            let mut odd_power = false;
            let p = self.factor_base[pi];
            while y2.remainder_digit(p) == 0 {
                odd_power ^= true;
                
                y2.div_by(p);
            }
            if odd_power {
                y2_in_factor_base.set_bit(pi);
            }

            if y2.cmp(p * p) == Ordering::Less {
                if let Some(q) = y2.to_digit() {
                    if 1 < q  &&  q <= self.factor_base_limit {
                        if let Some(qi) = self.factor_base[(pi+1)..].iter().position(|&x| x == q) {
                            y2.set_from(1);
                            y2_in_factor_base.set_bit(pi + 1 + qi);
                        } else {
                            println!("Error: y2 is below factor_base_limit, but it is not found in the factor base! {} {}", q, self.factor_base_limit);
                            return None;
                        }
                    }
                }
                break;
            }
        }

        return if y2.cmp(self.factor_base_limit * self.factor_base_limit) != Ordering::Greater {
            Some((y2_in_factor_base, y2.to_digit().unwrap()))
        } else {
            println!("Left over is {}", y2);
            None
        }
    }

}