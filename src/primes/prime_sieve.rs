

#[inline(always)]
fn ntoi(n: u64) -> (usize, usize) {
    let i = ((n - 1) / 2) as usize;
    (i / 8, i % 8)
}


#[inline(always)]
fn iton(i: usize, b: usize) -> u64 {
    ((i*8 + b) * 2 + 1) as u64
}


pub fn prime_sieve(limit: u64) -> Vec<u64> {
    // See https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
    let mut primes = Vec::new();

    if limit >= 2 {
        primes.push(2);
    }

    // Create a list of all the potential primes (only the odd ones)
    let mut n = Vec::new();
    n.resize((limit as usize + 15)/16, 0xffu8);

    // Remove 1 as it is not prime
    let (i, b) = ntoi(1);
    n[i] &= !(1<<b);

    'outer: for i in 0..n.len() {
        for b in 0..8 {
            if (n[i] & (1<<b)) != 0 {
                let prime = iton(i, b);
                primes.push(prime);

                let (mut ii, mut bb) = ntoi(prime * prime);
                if ii >= n.len() {
                    break 'outer;
                }

                // Mark all multiple of prime as composite starting at prime^2
                while ii < n.len() {
                    n[ii] &= !(1<<bb);
                    bb += prime as usize;
                    ii += bb / 8;
                    bb %= 8;
                }
            }
        }
    }

    // Add all remaining numbers in n as primes
    let mut num = primes[primes.len()-1] + 2;
    while num <= limit {
        let (i, b) = ntoi(num);
        if (n[i] & (1<<b)) != 0 {
            primes.push(num);
        }
        num += 2;
    }
        
    primes
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_prime_sieve() {
        // Prime numbers from https://en.wikipedia.org/wiki/Prime-counting_function
        // and https://en.wikipedia.org/wiki/List_of_prime_numbers
        let p = prime_sieve(10);
        assert_eq!(p, vec![2, 3, 5, 7]);

        let p = prime_sieve(7919);
        assert_eq!(p.len(), 1000);

        let p = prime_sieve(1000000);
        assert_eq!(p.len(), 78498);
    }

    #[test]
    fn test_prime_sieve_benchmark() {
        use std::time::Instant;
        let now = Instant::now();
        
        let p = prime_sieve(100000000);

        println!("test_prime_sieve_benchmark: {:?}", now.elapsed());
        assert_eq!(p.len(), 5761455);
    }


    #[test]
    fn test_prime_sieve_benchmark_2() {
        use std::time::Instant;
        let now = Instant::now();
        
        let p = prime_sieve(1000000000);

        println!("test_prime_sieve_benchmark: {:?}", now.elapsed());
        assert_eq!(p.len(), 50847534);
    }


    #[test]
    fn test_prime_sieve_benchmark_3() {
        use std::time::Instant;
        let now = Instant::now();
        
        let p = prime_sieve(10000000000);

        println!("test_prime_sieve_benchmark: {:?}", now.elapsed());
        assert_eq!(p.len(), 455052511);
    }
}
