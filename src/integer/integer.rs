
use std::cmp::Ordering;
use std::fmt;

use super::number::{Digit, Number};


#[derive(Debug, Clone, PartialEq)]
pub struct Integer {
    positive: bool,
    number: Number,
}


impl Integer {
    pub fn from(n: Digit) -> Integer {
        Integer {
            positive: true,
            number: Number::from(n)
        }
    }


    pub fn from_number(n: Number) -> Integer {
        Integer {
            positive: true,
            number: n
        }
    }


    pub fn from_string(n: &str) -> Result<Integer, String> {
        let mut num = Integer {
            positive: true,
            number: Number::from(0)
        };

        if n.starts_with("-") {
            num.positive = false;
            num.number = Number::from_string(&n[1..])?;
        } else {
            num.number = Number::from_string(n)?;
        }

        Ok(num)
    }


    pub fn from_random_n_bits(n: usize) -> Integer {
        Integer {
            positive: true,
            number: Number::gen_random_n_bits(n)
        }
    }


    pub fn set_from(&mut self, n: Digit) -> () {
        self.positive = true;
        self.number.set_from(n);
    }


    pub fn set_from_integer(&mut self, source: Integer) -> () {
        self.positive = source.positive;
        self.number = source.number;
    }


    pub fn to_digit(&self) -> Option<Digit> {
        return if self.positive {
            self.number.to_digit()
        } else {
            None
        }
    }


    pub fn get_number(&self) -> &Number {
        &self.number
    }


    pub fn negate(&mut self) -> () {
        self.positive ^= true;
    }


    pub fn is_positive(&self) -> bool {
        self.positive
    }

    
    pub fn is_even(&self) -> bool {
        self.number.is_even()
    }


    pub fn is_doubly_even(&self) -> bool {
        self.number.is_doubly_even()
    }


    pub fn popcnt(&self) -> Digit {
        self.number.popcnt()
    }


    pub fn increment(&mut self, step: Digit) -> () {
        if self.positive {
            self.number.increment(step);
        }
        else {
            // TODO this case is not handling overflows correctly!
            self.number.decrement(step);
            if self.number.cmp_digit(0) == Ordering::Equal { 
                self.positive = true;
            }
        }
    }


    pub fn decrement(&mut self, step: Digit) -> () {
        if self.positive {
            self.number.decrement(step);
        }
        else {
            // TODO this case is not handling overflows correctly!
            self.number.increment(step);
            if self.number.cmp_digit(0) == Ordering::Equal { 
                self.positive = true;
            }
        }
    }


    pub fn mul(&mut self, n: Digit) -> () {
        self.number.mul_digit(n);

        if self.number.cmp_digit(0) == Ordering::Equal {
            self.positive = true;
        }
    }


    pub fn div_by_2(&mut self) -> () {
        self.number.div_by_2();

        if self.number.cmp_digit(0) == Ordering::Equal {
            self.positive = true;
        }
    }


    pub fn div_by_4(&mut self) -> () {
        self.number.div_by_4();

        if self.number.cmp_digit(0) == Ordering::Equal {
            self.positive = true;
        }
    }

    pub fn div_by(&mut self, n: Digit) -> () {
        self.number.div_by(n);

        if self.number.cmp_digit(0) == Ordering::Equal {
            self.positive = true;
        }
    }


    pub fn div(&self, rhs: &Integer) -> (Integer, Integer) {
        // TODO Fix the remainders when handling negative numbers
        let (quot, rest) = self.number.div(&rhs.number);

        return (
            Integer {
                positive: self.positive == rhs.positive,
                number: quot
            },
            Integer {
                positive: self.positive == rhs.positive,
                number: rest
            }
        );
    }


    pub fn remainder(&self, n: &Integer) -> Integer {
        // TODO Fix the remainders when handling negative numbers
        Integer {
            positive: self.positive == n.positive,
            number: self.number.remainder(&n.number)
        }
    }


    pub fn remainder_digit(&self, n: Digit) -> Digit {
        // TODO Fix the remainders when handling negative numbers
        self.number.remainder_digit(n)
    }


    pub fn cmp(&self, n: Digit) -> Ordering {
        if self.positive {
            self.number.cmp_digit(n)
        } else {
            std::cmp::Ordering::Less
        }
    }


    pub fn set_bit(&mut self, bit: usize) -> () {
        self.number.set_bit(bit);
    }

    
    pub fn top_bit_index(&self) -> usize {
        self.number.top_bit_index()
    }


    pub fn is_zero(&self) -> bool {
        self.number.is_zero()
    }
}


impl fmt::Display for Integer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if !self.positive {
            write!(f, "-")?;
        }
        write!(f, "{}", self.number)
    }
}


impl fmt::Binary for Integer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if !self.positive {
            write!(f, "-")?;
        }
        write!(f, "{:b}", self.number)
    }
}


impl std::cmp::PartialOrd for Integer {
    fn partial_cmp(&self, other: &Integer) -> Option<Ordering> {
        Some(if self.positive != other.positive {
            match self.positive {
                true => Ordering::Greater,
                false => Ordering::Less,
            }
        }
        else if self.positive {
            self.number.cmp(&other.number)
        }
        else {
            match self.number.cmp(&other.number) {
                Ordering::Greater => Ordering::Less,
                Ordering::Less => Ordering::Greater,
                Ordering::Equal => Ordering::Equal
            }
        })
    }
}


impl std::ops::BitAnd<Digit> for &Integer {
    type Output = Digit;

    fn bitand(self, other: Digit) -> Digit {
        self.number.get_nth_digit(0) & other
    }
}


impl std::ops::BitAnd<&Integer> for &Integer {
    type Output = Integer;

    fn bitand(self, other: &Integer) -> Integer {
        let mut tmp = Integer {
            positive: self.positive,
            number: self.number.clone()
        };
        tmp.number.bitand(&other.number);
        tmp
    }
}


impl std::ops::BitXor<&Integer> for &Integer {
    type Output = Integer;

    fn bitxor(self, other: &Integer) -> Integer {
        let mut tmp = Integer {
            positive: self.positive,
            number: self.number.clone()
        };
        tmp.number.bitxor(&other.number);
        tmp
    }
}


impl std::ops::Shr<Digit> for &Integer {
    type Output = Integer;

    fn shr(self, other: Digit) -> Integer {
        let mut n = self.clone();

        n.number.shift_right(other);
        if n.number.is_zero() { 
            n.positive = true;
        }
        n
    }
}


impl std::ops::Add<&Integer> for &Integer {
    type Output = Integer;

    fn add(self, rhs: &Integer) -> Self::Output {

        return if self.positive == rhs.positive {
            let mut tmp = Integer {
                positive: self.positive,
                number: self.number.clone()
            };
            tmp.number.add(&rhs.number);
            tmp
        }
        else if self.positive {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => Integer {
                        positive: true,
                        number: Number::from(0)
                    },
                Ordering::Greater => {
                    let mut tmp = Integer {
                        positive: true,
                        number: self.number.clone()
                    };
                    tmp.number.sub(&rhs.number);
                    tmp
                },
                Ordering::Less => {
                    let mut tmp = Integer {
                        positive: false,
                        number: rhs.number.clone()
                    };
                    tmp.number.sub(&self.number);
                    tmp
                }
            }
        }
        else
        {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => Integer {
                        positive: true,
                        number: Number::from(0)
                    },
                Ordering::Greater => {
                    let mut tmp = Integer {
                        positive: false,
                        number: self.number.clone()
                    };
                    tmp.number.sub(&rhs.number);
                    tmp
                },
                Ordering::Less => {
                    let mut tmp = Integer {
                        positive: true,
                        number: rhs.number.clone()
                    };
                    tmp.number.sub(&self.number);
                    tmp
                }
            }
        }
    }
}


impl std::ops::AddAssign<&Integer> for Integer {
    fn add_assign(&mut self, rhs: &Integer) {
        if self.positive == rhs.positive {
            self.number.add(&rhs.number)
        }
        else if self.positive {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => self.set_from(0),
                Ordering::Greater => self.number.sub(&rhs.number),
                Ordering::Less => {
                    self.positive = false;
                    let mut tmp = rhs.number.clone();
                    tmp.sub(&self.number);
                    self.number = tmp;
                }
            };
        }
        else
        {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => self.set_from(0),
                Ordering::Greater => self.number.sub(&rhs.number),
                Ordering::Less => {
                    self.positive = true;
                    let mut tmp = rhs.number.clone();
                    tmp.sub(&self.number);
                    self.number = tmp;
                }
            };
            self.positive = false;
        }
    }
}


impl std::ops::Sub<&Integer> for &Integer {
    type Output = Integer;

    fn sub(self, rhs: &Integer) -> Self::Output {
        return if self.positive != rhs.positive {
            let mut tmp = Integer {
                positive: self.positive,
                number: self.number.clone()
            };
            tmp.number.add(&rhs.number);
            tmp
        }
        else if self.positive {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => Integer {
                        positive: true,
                        number: Number::from(0)
                    },
                Ordering::Greater => {
                    let mut tmp = Integer {
                        positive: true,
                        number: self.number.clone()
                    };
                    tmp.number.sub(&rhs.number);
                    tmp
                },
                Ordering::Less => {
                    let mut tmp = Integer {
                        positive: false,
                        number: rhs.number.clone()
                    };
                    tmp.number.sub(&self.number);
                    tmp
                }
            }
        }
        else
        {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => Integer {
                        positive: true,
                        number: Number::from(0)
                    },
                Ordering::Greater => {
                    let mut tmp = Integer {
                        positive: false,
                        number: self.number.clone()
                    };
                    tmp.number.sub(&rhs.number);
                    tmp
                },
                Ordering::Less => {
                    let mut tmp = Integer {
                        positive: true,
                        number: rhs.number.clone()
                    };
                    tmp.number.sub(&self.number);
                    tmp
                }
            }
        }
    }
}


impl std::ops::SubAssign for Integer {
    fn sub_assign(&mut self, rhs: Integer) -> () {
        if self.positive != rhs.positive {
            self.number.add(&rhs.number)
        }
        else if self.positive {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => self.set_from(0),
                Ordering::Greater => self.number.sub(&rhs.number),
                Ordering::Less => {
                    self.positive = false;
                    let mut tmp = rhs.number.clone();
                    tmp.sub(&self.number);
                    self.number = tmp;
                }
            }
        }
        else
        {
            match self.number.cmp(&rhs.number) {
                Ordering::Equal => self.set_from(0),
                Ordering::Greater => self.number.sub(&rhs.number),
                Ordering::Less => {
                    self.positive = true;
                    let mut tmp = rhs.number.clone();
                    tmp.sub(&self.number);
                    self.number = tmp;
                }
            }
        }
    }
}


impl std::ops::Mul<&Integer> for &Integer {
    type Output = Integer;

    fn mul(self, rhs: &Integer) -> Self::Output {
        Integer {
            positive: self.positive == rhs.positive,
            number: self.number.mul(&rhs.number)
        }
    }
}


impl std::ops::Div<&Integer> for &Integer {
    type Output = Integer;

    fn div(self, rhs: &Integer) -> Self::Output {
        let (quot, _rest) = self.number.div(&rhs.number);

        Integer {
            positive: self.positive == rhs.positive,
            number: quot
        }
    }
}


impl std::ops::Rem<&Integer> for &Integer {
    type Output = Integer;

    fn rem(self, rhs: &Integer) -> Self::Output {
        let (_quot, rest) = self.number.div(&rhs.number);

        Integer {
            positive: self.positive == rhs.positive,
            number: rest
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_creation() {
        let a = Integer::from(100);
        let mut b = Integer::from_string("100").unwrap();
        let mut c = Integer::from_string("-100").unwrap();

        assert_eq!(a, b);

        b.negate();
        assert_eq!(b, c);

        c.negate();
        assert_eq!(a, c);
    }


    #[test]
    fn test_creation_failure() {
        match Integer::from_string("1a00") {
            Ok(_) => assert!(false),
            Err(_) => assert!(true)
        };

        match Integer::from_string("--100") {
            Ok(_) => assert!(false),
            Err(_) => assert!(true)
        };
    }


    #[test]
    fn test_add() {
        let a = Integer::from(200);
        let b = Integer::from(80);
        let c = Integer::from_string("280").unwrap();
        assert_eq!(&a + &b, c);

        let d = Integer::from_string("-300").unwrap();
        let e = Integer::from_string("-100").unwrap();
        assert_eq!(&a + &d, e);
    }


    #[test]
    fn test_sub() {
        let a = Integer::from(200);
        let b = Integer::from(80);
        let c = Integer::from_string("-120").unwrap();
        assert_eq!(&b - &a, c);

    }

    
    #[test]
    fn test_mul() {
        let mut a = Integer::from_string("500").unwrap();
        let b = Integer::from_string("1000").unwrap();
        let c = Integer::from_string("500000").unwrap();
        assert_eq!(&a * &b, c);

        let d = 85;
        let id = Integer::from(d);
        let r = &a * &id;
        a.mul(d);
        assert_eq!(a, r);
    }


    #[test]
    fn test_div() {
        let a = Integer::from_string("20000").unwrap();
        let b = Integer::from_string("6500").unwrap();
        let c = Integer::from_string("3").unwrap();
        let d = Integer::from_string("500").unwrap();
        assert_eq!(&a / &b, c);
        assert_eq!(&a % &b, d);
    }


    #[test]
    #[allow(unused)]
    #[should_panic]
    fn test_div_by_zero() {
        let a = Integer::from_string("20000").unwrap();
        let b = Integer::from_string("0").unwrap();
        
        &a / &b;
    }


    #[test]
    fn test_pop_cnt() {
        let mut bits = 5;
        let mut a = Integer::from_string("32").unwrap();
        let one = Integer::from(1);

        for _ in 0..4 {
            a = &a* &a;
            bits *= 2;
            let n_bits = &a - &one;
            assert_eq!(n_bits.popcnt(), bits);
        }
    }
}
