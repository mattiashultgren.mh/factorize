
use std::cmp::Ordering;
use std::fmt;

use rand::Rng;


pub type Digit = u64;
pub type DoubleDigit = u128;

static BIT_SIZE: Digit = 64;
static MASK: DoubleDigit = ((1 as DoubleDigit) << BIT_SIZE) - 1;


#[derive(Debug, Clone, PartialEq)]
#[must_use = "You created it, so use it!"]
pub struct Number {
    digits: Vec<Digit>,
}


impl Number {
    pub fn from(n: Digit) -> Number {
        Number {
            digits: if n > 0 { vec![n] } else { Vec::new() }
        }
    }


    pub fn from_vec(v: Vec<Digit>) -> Number {
        let mut num = Number {
            digits: v
        };

        while num.digits[num.digits.len() - 1] == 0 {
            num.digits.truncate(num.digits.len() - 1);
        }

        num
    }


    pub fn from_string(n: &str) -> Result<Number, String> {
        let mut num = Number::from(0);

        for dc in n.chars() {
            let d: Digit = match dc {
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '_' => continue,
                _ => return Err(format!("Invalid character ({})", dc))
            };

            num.mul_digit(10);
            num.add_offset_digit(d, 0);
        }

        while num.digits[num.digits.len() - 1] == 0 {
            num.digits.truncate(num.digits.len() - 1);
        }

        Ok(num)
    }


    pub fn set_from(&mut self, n: Digit) -> () {
        if n == 0 {
            self.digits.truncate(0);
        } else {
            self.digits.truncate(1);
            self.digits[0] = n;
        }
    }


    pub fn to_digit(&self) -> Option<Digit> {
        return if self.digits.len() == 0 {
            Some(0)
        } else if self.digits.len() == 1 {
            Some(self.digits[0])
        } else {
            None
        }
    }


    pub fn get_nth_digit(&self, n: usize) -> Digit {
        if n >= self.digits.len() {
            0
        } else {
            self.digits[n]
        }
    }


    pub fn trailing_zeros(&self) -> Digit {
        if self.digits.len() == 0 {
            return 0;
        }

        for idx in 0..self.digits.len() {
            if self.digits[idx] != 0 {
                return BIT_SIZE * (idx as Digit) + (self.digits[idx].trailing_zeros() as Digit);
            }
        }
        assert!(false, "Code should never reach this point!");
        1
    }


    pub fn cmp(&self, b: &Number) -> Ordering {
        if self.digits.len() == b.digits.len() {
            for i in (0..self.digits.len()).rev() {
                if self.digits[i] == b.digits[i] {
                    continue;
                }
                return if self.digits[i] < b.digits[i] {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            }
            return Ordering::Equal;
        }
        return if self.digits.len() < b.digits.len() {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }


    fn cmp_offset(&self, b: &Number, offset: usize) -> Ordering {
        if self.digits.len() == (b.digits.len() + offset) {
            for i in (0..b.digits.len()).rev() {
                if self.digits[offset + i] == b.digits[i] {
                    continue;
                }
                return if self.digits[offset + i] < b.digits[i] {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            }

            for i in 0..offset {
                if self.digits[i] > 0 {
                    return Ordering::Greater;
                }
            }
            return Ordering::Equal;
        }
        return if self.digits.len() < (b.digits.len() + offset) {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }


    pub fn cmp_digit(&self, n: Digit) -> Ordering {
        match self.digits.len() {
            0 => 0.cmp(&n),
            1 => self.digits[0].cmp(&n),
            _ => std::cmp::Ordering::Greater
        }
    }


    pub fn is_zero(&self) -> bool {
        self.digits.len() == 0
    }


    pub fn is_even(&self) -> bool {
        return if self.digits.len() == 0 {
            true
        } else {
            (self.digits[0] & 1) != 1
        };
    }


    pub fn is_doubly_even(&self) -> bool {
        return if self.digits.len() == 0 {
            true
        } else {
            (self.digits[0] & 3) == 0
        };
    }


    pub fn set_bit(&mut self, bit: usize) -> () {
        let digit_index = bit / (BIT_SIZE as usize);

        if digit_index >= self.digits.len() {
            self.digits.resize(digit_index + 1, 0);
        }

        self.digits[digit_index] |= 1 << (bit % (BIT_SIZE as usize));
    }


    pub fn clear_bit_no_truncate(&mut self, bit: usize) -> () {
        let digit_index = bit / (BIT_SIZE as usize);

        if digit_index < self.digits.len() {
            self.digits[digit_index] &= !(1 << (bit % (BIT_SIZE as usize)));
        }
    }

    pub fn clear_bit(&mut self, bit: usize) -> () {
        self.clear_bit_no_truncate(bit);

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn is_bit_set(&self, n: usize) -> bool {
        let (i, b) = (n / (BIT_SIZE as usize), n % (BIT_SIZE as usize));

        return if i >= self.digits.len() {
            false
        } else {
            (self.digits[i] & (1 << b)) != 0
        };
    }


    pub fn top_bit_index(&self) -> usize {
        if self.digits.len() == 0 {
            return 0;
        }
        let index_base = (BIT_SIZE as usize) * (self.digits.len() - 1);
        let mut index = index_base;

        for i in 0..64 {
            if ((1 << i) & self.digits[self.digits.len() - 1]) != 0 {
                index = index_base + i;
            }
        }
        index
    }


    pub fn popcnt(&self) -> Digit {
        let mut count = 0;
        let mut n = self.clone();

        let one = Number::from(1);

        while !n.is_zero() {
            let mut tmp = n.clone();
            tmp.sub(&one);
            n.bitand(&tmp);
            count += 1;
        }
        count
    }


    pub fn div_by_2(&mut self) -> () {
        let len = self.digits.len();

        if len == 0 {
            return;
        }
        
        for i in 0..(len - 1) {
            self.digits[i] = (self.digits[i] >> 1) | ((self.digits[i+1] & 1) << (BIT_SIZE - 1))
        }
        self.digits[len - 1] = self.digits[len - 1] >> 1;

        if self.digits[len - 1] == 0 {
            self.digits.truncate(len - 1);
        }
    }


    pub fn div_by_4(&mut self) -> () {
        let len = self.digits.len();

        if len == 0 {
            return;
        }
        
        for i in 0..(len - 1) {
            self.digits[i] = (self.digits[i] >> 2) | ((self.digits[i+1] & 3) << (BIT_SIZE - 2))
        }
        self.digits[len - 1] = self.digits[len - 1] >> 2;

        if self.digits[len - 1] == 0 {
            self.digits.truncate(len - 1);
        }
    }


    pub fn div_by(&mut self, n: Digit) -> () {

        let mut rest = 0;
        for idx in (0..self.digits.len()).rev() {
            let num = ((rest as DoubleDigit) << BIT_SIZE) + (self.digits[idx] as DoubleDigit);
            self.digits[idx] = (num / (n as DoubleDigit)) as Digit;
            rest = (num % (n as DoubleDigit)) as Digit;
        }

        if self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn shift_right(&mut self, mut steps: Digit) -> () {
        // TODO: Fix this lazy implementation to something faster.
        //       Currently this is an O(n^2) method when it really should be O(n)
        while steps >= 2 {
            steps -= 2;
            self.div_by_4();
        }
        if steps == 1 {
            self.div_by_2();
        }
    }


    pub fn shift_left(&mut self, mut steps: Digit) -> () {
        // TODO: Fix this lazy implementation to something faster.
        //       Currently this is an O(n^2) method when it really should be O(n)
        while steps >= 8 {
            steps -= 8;
            self.mul_digit(256);
        }
        while steps >= 2 {
            steps -= 2;
            self.mul_digit(4);
        }
        if steps == 1 {
            self.mul_digit(2);
        }
    }


    pub fn bitand(&mut self, b: &Number) -> () {
        self.digits.truncate(b.digits.len());
        
        for i in 0..self.digits.len() {
            self.digits[i] &= b.digits[i];
        }

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn bitor(&mut self, b: &Number) -> () {
        let len = std::cmp::max(self.digits.len(), b.digits.len());
        self.digits.resize(len, 0);

        for i in 0..b.digits.len() {
            self.digits[i] |= b.digits[i];
        }
    }


    pub fn bitxor(&mut self, b: &Number) -> () {
        let len = std::cmp::max(self.digits.len(), b.digits.len());
        self.digits.resize(len, 0);

        for i in 0..b.digits.len() {
            self.digits[i] ^= b.digits[i];
        }

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn bitand_is_zero(&self, b: &Number) -> bool {
        for i in 0..std::cmp::min(self.digits.len(), b.digits.len()) {
            if (self.digits[i] & b.digits[i]) != 0 {
                return false;
            }
        }
        return true;
    }


    pub fn increment(&mut self, step: Digit) -> () {
        let mut carry = step;

        for i in 0..self.digits.len() {
            let sum = (self.digits[i] as DoubleDigit) + (carry as DoubleDigit);

            self.digits[i] = (sum & MASK) as Digit;
            carry = (sum >> BIT_SIZE) as Digit;
        }
        
        if carry != 0 {
            self.digits.push(carry);
        }
    }


    pub fn decrement(&mut self, step: Digit) -> () {
        let borrow_digit: DoubleDigit = 1 << BIT_SIZE;
        let mut borrow = step;

        for i in 0..self.digits.len() {
            let diff = (self.digits[i] as DoubleDigit) + borrow_digit - (borrow as DoubleDigit);

            self.digits[i] = (diff & MASK) as Digit;
            borrow = 1 - ((diff >> BIT_SIZE) as Digit);
        }
        
        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }

        assert!(borrow == 0);
    }


    pub fn add(&mut self, b: &Number) -> () {
        self.digits.resize(std::cmp::max(self.digits.len(), b.digits.len()) + 1, 0);

        let mut carry: Digit = 0;
        for i in 0..b.digits.len() {
            let sum = (self.digits[i] as DoubleDigit) + (b.digits[i] as DoubleDigit) + (carry as DoubleDigit);

            self.digits[i] = (sum & MASK) as Digit;
            carry = (sum >> BIT_SIZE) as Digit;
        }
        for i in b.digits.len()..self.digits.len() {
            let sum = (self.digits[i] as DoubleDigit) + (carry as DoubleDigit);
            self.digits[i] = (sum & MASK) as Digit;
            carry = (sum >> BIT_SIZE) as Digit;
        }

        assert!(carry == 0);

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn add_offset_digit(&mut self, n: Digit, offset: usize) -> () {
        while offset >= self.digits.len()  {
            self.digits.push(0)
        }

        let sum = (self.digits[offset] as DoubleDigit) + (n as DoubleDigit);
        self.digits[offset] = (sum & MASK) as Digit;
        let mut carry = (sum >> BIT_SIZE) as Digit;

        if self.digits.len() > offset + 1 {
            for i in (offset+1)..self.digits.len() {
                let sum = (self.digits[i] as DoubleDigit) + (carry as DoubleDigit);
                self.digits[i] = (sum & MASK) as Digit;
                carry = (sum >> BIT_SIZE) as Digit;
            }
        }

        if carry != 0 {
            self.digits.push(carry);
        }
    }


    /// Subtracts b from a and returns the result.
    /// Note: a must be greater than b.
    pub fn sub(&mut self, b: &Number) -> () {
        assert!(self.digits.len() >= b.digits.len());

        let borrow_digit: DoubleDigit = 1 << BIT_SIZE;

        let mut borrow: Digit = 0;
        for i in 0..b.digits.len() {
            let diff = (self.digits[i] as DoubleDigit) + borrow_digit -
                             (b.digits[i] as DoubleDigit) - (borrow as DoubleDigit);

            self.digits[i] =  (diff & MASK) as Digit;
            borrow = 1 - ((diff >> BIT_SIZE) as Digit);
        }
        if self.digits.len() > b.digits.len() {
            for i in b.digits.len()..self.digits.len() {
                let diff = (self.digits[i] as DoubleDigit) + borrow_digit - (borrow as DoubleDigit);

                self.digits[i] = (diff & MASK) as Digit;
                borrow = 1 - ((diff >> BIT_SIZE) as Digit);
            }
        }

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }

        assert!(borrow == 0);
    }


    /// Sutracts b * n * 2^(BIT_SIZE * offset) from self
    /// Note: Self must be greater as no underflow is allowed
    fn sub_number_mul_offset_digit(&mut self, b: &Number, n: Digit, offset: usize) -> () {
        assert!(self.digits.len() >= (b.digits.len() + offset));

        let n = n as DoubleDigit;

        let borrow_digit: DoubleDigit = MASK << BIT_SIZE;
        let mut borrow = 0;
        for i in 0..b.digits.len() {
            let diff = (self.digits[offset + i] as DoubleDigit) + borrow_digit -
                             n * (b.digits[i] as DoubleDigit) - (borrow as DoubleDigit);
            
            self.digits[offset + i] = (diff & MASK) as Digit;
            borrow = (MASK as Digit) - ((diff >> BIT_SIZE) as Digit);
        }

        if self.digits.len() > (b.digits.len() + offset) {
            let borrow_digit: DoubleDigit = 1 << BIT_SIZE;
            for i in (b.digits.len() + offset)..self.digits.len() {
                let diff = (self.digits[i] as DoubleDigit) + borrow_digit - (borrow as DoubleDigit);

                self.digits[i] = (diff & MASK) as Digit;
                borrow = 1 - ((diff >> BIT_SIZE) as Digit);
            }
        }

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }

        assert!(borrow == 0);
    }


    pub fn mul_digit(&mut self, n: Digit) -> () {
        let mut carry = 0;
        for ai in 0..self.digits.len() {
            let prod = (self.digits[ai] as DoubleDigit) * (n as DoubleDigit) + (carry as DoubleDigit);

            self.digits[ai] = (prod & MASK) as Digit;
            carry = (prod >> BIT_SIZE) as Digit;
        }

        if carry != 0 {
            self.digits.push(carry);
        }

        while self.digits.len() > 0  &&  self.digits[self.digits.len() - 1] == 0 {
            self.digits.truncate(self.digits.len() - 1);
        }
    }


    pub fn mul(&self, b: &Number) -> Number {
        let mut r = Number::from(0);
        r.digits.resize(self.digits.len() + b.digits.len(), 0);

        for si in 0..self.digits.len() {
            let mut carry = 0;
            for bi in 0..b.digits.len() {
                let prod = (self.digits[si] as DoubleDigit) * (b.digits[bi] as DoubleDigit) +
                                 (carry as DoubleDigit) + (r.digits[si + bi] as DoubleDigit);

                r.digits[si + bi] = (prod & MASK) as u64;
                carry = (prod >> BIT_SIZE) as u64;
            }

            let mut i = si + b.digits.len();
            while carry > 0 {
                let sum = (r.digits[i] as DoubleDigit) + (carry as DoubleDigit);

                r.digits[i] = (sum & MASK) as Digit;
                carry = (sum >> BIT_SIZE) as Digit;
                i += 1;
            }
        }

        while r.digits.len() > 0  &&  r.digits[r.digits.len() - 1] == 0 {
            r.digits.truncate(r.digits.len() - 1);
        }

        r
    }


    pub fn div(&self, b: &Number) -> (Number, Number) {
        if b.digits.len() == 0 { 
            panic!("Division by zero");
        }
        else if self.digits.len() == 1  && b.digits.len() == 1 {
            return (
                Number::from(self.digits[0] / b.digits[0]),
                Number::from(self.digits[0] % b.digits[0])
            )
        }

        let mut rest = self.clone();
        let mut quot = Number::from(0);

        while rest.cmp(&b) == Ordering::Less {
            return (quot, rest);
        }

        let divider = (b.digits[b.digits.len() - 1] as DoubleDigit) + 1;
        let mut offset = self.digits.len() - b.digits.len();

        while rest.cmp(&b) != Ordering::Less {
            while rest.cmp_offset(&b, offset) == Ordering::Less {
                offset -= 1;
            }

            let mut approx_mult = if rest.digits.len() > (b.digits.len() + offset) {
                ((
                    ((rest.digits[rest.digits.len() - 1] as DoubleDigit) << BIT_SIZE) +
                    (rest.digits[rest.digits.len() - 2] as DoubleDigit)
                ) / divider) as Digit
            } else {
                ((rest.digits[rest.digits.len() - 1] as DoubleDigit) / divider) as Digit
            };

            if approx_mult == 0 {
                approx_mult = 1;
            }

            rest.sub_number_mul_offset_digit(&b, approx_mult, offset);

            quot.add_offset_digit(approx_mult, offset);
        }

        (quot, rest)
    }


    pub fn remainder(&self, n: &Number) -> Number {
        if n.digits.len() == 1 {
            return Number::from(self.remainder_digit(n.digits[0]));
        } else {
            let (_qout, rest) = self.div(&n);
            return rest;
        }
    }

    pub fn remainder_digit(&self, n: Digit) -> Digit {
        let conv = (((1 as DoubleDigit) << BIT_SIZE) % n as DoubleDigit) as Digit;
        let mut factor: Digit = 1;
        let mut rest: Digit = 0;
        for d in self.digits.iter() {
            rest = (((factor as DoubleDigit) * (*d as DoubleDigit) + rest as DoubleDigit) % (n as DoubleDigit)) as Digit;
            factor = (((factor as DoubleDigit) * (conv as DoubleDigit)) % (n as DoubleDigit)) as Digit;
        }
        return rest;
    }


    pub fn gen_random_n_bits(n: usize) -> Number {
        let mut rng = rand::thread_rng();
        let mut data: Number = Number::from(0);

        let bit_size = BIT_SIZE as usize;

        data.digits.resize((n + (bit_size -1)) / bit_size, 0);

        for i in 0..data.digits.len() {
            data.digits[i] = rng.gen();
        }

        // Mask away any potential higher bits, and mark the top bit
        let len = data.digits.len();
        data.digits[len - 1] &= (1 << ((n-1) & (bit_size -1))) - 1;
        data.digits[len - 1] |= 1 << ((n-1) & (bit_size-1));

        data
    }

    
    pub fn gen_random_sparse_n_bits(n: usize, set_bits: usize) -> Number {
        let mut rng = rand::thread_rng();
        let mut data = Number::from(0);

        for _ in 0..set_bits {
            let bit = rng.gen_range(0..n);
            data.set_bit(bit);
        }

        data
    }


    pub fn isqrt(&self) -> Number {
        let mut x = Number::from(0);

        // Use the f64.sqrt() as a first guess for the iterative process
        match self.digits.len() {
            0 => return x,
            1 => x.add_offset_digit(((self.digits[0] as f64).sqrt()) as Digit, 0),
            2 => {
                let mut n = (self.digits[1] as f64) * 2.0f64.powi(BIT_SIZE as i32);
                n += self.digits[0] as f64;

                x.add_offset_digit(n.sqrt() as Digit, 0);
            },
            _ => {
                let mut n = (self.digits[self.digits.len() - 1] as f64) * 2.0f64.powi(BIT_SIZE as i32);
                n += self.digits[self.digits.len() - 2] as f64;

                if self.digits.len() & 1 == 1 {
                    n *= 2.0f64.powi(BIT_SIZE as i32);
                    let sqrt = n.sqrt() as DoubleDigit;

                    x.add_offset_digit((sqrt & MASK) as Digit, (self.digits.len() - 3) / 2);
                    x.add_offset_digit((sqrt >> BIT_SIZE) as Digit, (self.digits.len() - 3) / 2 + 1);
                } else {
                    x.add_offset_digit(n.sqrt() as Digit, (self.digits.len() - 2) / 2);
                }
            },
        }

        // Method from https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method
        loop {
            let (mut next_x, _) = self.div(&x);
            next_x.add(&x);
            next_x.div_by_2();
    
            if x.cmp(&next_x) == Ordering::Greater {
                let mut diff = x.clone();
                diff.sub(&next_x);
                if diff.cmp_digit(2) == Ordering::Less {
                    break;
                }
            } else {
                let mut diff = next_x.clone();
                diff.sub(&x);
                if diff.cmp_digit(2) == Ordering::Less {
                    break;
                }
            };
    
            x = next_x;
        }
    
        loop {
            let mut x1 = x.clone();
            x1.increment(1);
            let square = x1.mul(&x1);
    
            if square.cmp(self) == Ordering::Greater {
                break;
            }
            x = x1;
        }
        loop {
            let square = x.mul(&x);
    
            if square.cmp(self) != Ordering::Greater {
                break;
            }
            x.decrement(1);
        }
    
        x
    
    }
}


impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.digits.len() == 0 {
            return write!(f, "0");
        }

        let ten = Number::from(10);
        let mut base = Number::from(1);
        let mut rest = self.clone();

        while base.cmp(&rest) != Ordering::Greater {
            base.mul_digit(10);
        }
        (base, _) = base.div(&ten);

        while matches!(base.cmp_digit(1), std::cmp::Ordering::Greater | std::cmp::Ordering::Equal) {
            let (digit, _)= rest.div(&base);

            let tmp = digit.mul(&base);
            rest.sub(&tmp);

            write!(f, "{}", if digit.digits.len() == 0 { 0 } else { digit.digits[0] })?;

            (base, _) = base.div(&ten);
        }
        Result::Ok(())
    }
}


impl fmt::Binary for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.digits.len() == 0 {
            return write!(f, "0");
        }

        let mut index = 0;
        match BIT_SIZE {
            64 => {
                for digit in self.digits.iter().rev() {
                    if index == 0 {
                        write!(f, "{:b}", digit)?;
                    } else {
                        write!(f, "{:064b}", digit)?;
                    }

                    index += 1;
                }
            },
            _ => panic!("Not implemented!")
        };
        Result::Ok(())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_offset_digit() {
        let mut a = Number::from_vec(vec![1, 2, 3]);
        a.add_offset_digit(4, 3);
        println!("{:?}", a);
        assert_eq!(a.cmp(&Number::from_vec(vec![1,2,3,4])), Ordering::Equal);

        a.add_offset_digit(1, 10);
        println!("{:?}", a);
        assert_eq!(a.cmp(&Number::from_vec(vec![1,2,3,4,0,0,0,0,0,0,1])), Ordering::Equal);

        a.add_offset_digit(MASK as u64, 10);
        println!("{:?}", a);
        assert_eq!(a.cmp(&Number::from_vec(vec![1,2,3,4,0,0,0,0,0,0,0,1])), Ordering::Equal);

        let mut a = Number::from_vec(vec![1,2,MASK as u64, MASK as u64, MASK as u64]);
        a.add_offset_digit(1, 2);
        println!("{:?}", a);
        assert_eq!(a.cmp(&Number::from_vec(vec![1,2,0,0,0,1])), Ordering::Equal);
    }
}
