
mod number;
mod integer;

pub use number::{DoubleDigit, Digit, Number};
pub use integer::Integer;


