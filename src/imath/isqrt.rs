
use crate::integer::Integer;


impl Integer {
    pub fn isqrt(&self) -> Integer {
        if !self.is_positive() {
            panic!("Trying isqrt on negative number!");
        }
    
        Integer::from_number(self.get_number().isqrt())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_isqrt() {
        let r999 = Integer::from_string("999").unwrap();
        let r1000 = Integer::from_string("1000").unwrap();
        let r1001 = Integer::from_string("1001").unwrap();

        let a = Integer::from_string("999999").unwrap();

        assert_eq!(a.isqrt(), r999);

        let a = Integer::from_string("1000000").unwrap();
        assert_eq!(a.isqrt(), r1000);

        let a = Integer::from_string("1002000").unwrap();
        assert_eq!(a.isqrt(), r1000);

        let a = Integer::from_string("1002001").unwrap();
        assert_eq!(a.isqrt(), r1001);
    }


    #[test]
    fn test_isqrt_benchmark_small() {
        use std::time::Instant;

        let root = Integer::from(654);

        let n = &root * &root;

        let now = Instant::now();

        let mut a= Integer::from(0);
        for _ in 0..10000000 {
            a = &a + &n.isqrt();
        }

        println!("test_isqrt_benchmark_small: {:?} for {} bit isqrt", now.elapsed(), n.top_bit_index()+1);

        assert!(a > n);
        assert_eq!(root, n.isqrt());
    }


    #[test]
    fn test_isqrt_benchmark_big() {
        use std::time::Instant;

        let n = Integer::from_string(
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
        ).unwrap();

        let n = &(&n * &(&n * &n)) * &(&n * &n);
        let n = &(&n * &(&n * &n)) * &(&n * &n);
        let n = &(&n * &n) * &(&n * &n);

        let root = &(&n * &(&n * &n)) * &(&n * &n);
        let n = &root * &root;

        let now = Instant::now();

        let calc_root = n.isqrt();
        println!("test_isqrt_benchmark_big: {:?} for {} bit isqrt", now.elapsed(), n.top_bit_index()+1);

        assert_eq!(root, calc_root);
    }


    #[test]
    #[allow(unused)]
    #[should_panic]
    fn test_negative_isqrt() {
        let neg_n = Integer::from_string("-10").unwrap();

        neg_n.isqrt();
        ()
    }
}
