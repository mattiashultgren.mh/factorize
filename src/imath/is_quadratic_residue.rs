
use std::cmp::Ordering;

use crate::integer::{Digit, Integer};
use super::{powmod, powmod_digit};



/// Computes if n is quadratic residue modulo a prime p.
/// I.e. does x^2 = n (mod p) exist.
/// Note: Parameter p must be a prime number!
pub fn is_quadratic_residue(n: &Integer, p: &Integer) -> bool {
    // Uses Euler's criterion as found at https://en.wikipedia.org/wiki/Euler%27s_criterion
    if p.cmp(2) == Ordering::Equal {
        return true;
    } else {
        let mut e = p.clone();
        e.decrement(1);
        e.div_by_2();
        let x = powmod(&n, &e, &p);

        return x.cmp(1) == Ordering::Equal;
    }
}


/// Computes if n is quadratic residue modulo a prime p.
/// I.e. does x^2 = n (mod p) exist.
/// Note: Parameter p must be a prime number!
pub fn is_quadratic_residue_digit(n: Digit, p: Digit) -> bool {
    // Uses Euler's criterion as found at https://en.wikipedia.org/wiki/Euler%27s_criterion
    if p == 2 {
        return true;
    } else {
        let e = (p - 1) / 2;
        let x = powmod_digit(n, e, p);

        return x == 1;
    }
}

