
use std::cmp::Ordering;

use crate::integer::Integer;


pub fn mod_inverse(a: &Integer, m: &Integer) -> Option<Integer> {
    // For details see https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    // and https://en.wikipedia.org/wiki/Modular_multiplicative_inverse#Extended_Euclidean_algorithm

    let mut a = a.clone();
    let mut b = m.clone();
    let mut x0 = Integer::from(1);
    let mut x1 = Integer::from(0);
    let mut y0 = Integer::from(0);
    let mut y1 = Integer::from(1);

    loop {
        let q = &a / &b;
        a = a.remainder(&b);

        x0 = &x0 - &(&q * &x1);
        y0 = &y0 - &(&q * &y1);

        if a.is_zero() {
            return if b.cmp(1) == Ordering::Equal {
                Some((&x1 + &m).remainder(&m))
            } else {
                None
            };
        }

        let q = &b / &a;
        b = b.remainder(&a);

        x1 = &x1 - &(&q * &x0);
        y1 = &y1 - &(&q * &y0);

        if b.is_zero() {
            return if a.cmp(1) == Ordering::Equal {
                Some((&x0 + &m).remainder(&m))
            } else {
                None
            };
        }

    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mod_inverse() {
        let m_digit = 137;
        let m = Integer::from(m_digit);

        for a in 1..m_digit {
            let a = Integer::from(a);

            if let Some(a_inv) = mod_inverse(&a, &m) {
                println!("mod_inverse({}, {}) => {}", a, m, a_inv);
                assert_eq!((&a * &a_inv).remainder(&m), Integer::from(1));
            } else {
                panic!("mod_inverse({}, {}) failed!", a, m);
            }
        }
    }
}
