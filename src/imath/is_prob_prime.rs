
use std::cmp::Ordering;

use crate::integer::{Digit, Integer};
use crate::imath::powmod;

#[derive(Debug)]
pub enum PrimeResult {
    Prime,
    ProbablePrime,
    NotPrimeIsBelowTwo,
    CompositeWitness(Integer),
    CompositeFactor(Integer)
}


fn is_witness(n: &Integer, d: &Integer, s: Digit, a: &Integer) -> bool {
    let mut x = powmod(&a, &d, &n);

    let mut neg_1 = n.clone();
    neg_1.decrement(1);

    let mut index = 0;
    while index < s {
        index += 1;

        let y = &(&x * &x) % &n;

        if (y.cmp(1) == Ordering::Equal)  &&  (x.cmp(1) != Ordering::Equal)  &&  (x != neg_1) {
            return true;
        }

        x = y;
    }
    if x.cmp(1) != Ordering::Equal {
        return true;
    }
    false
}


pub fn is_prob_prime(n: &Integer) -> PrimeResult {
    // For details see https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
    // Note the following list of primes makes the primality test fully deterministic
    // for n < 3,317,044,064,679,887,385,961,981. Above that the test is just very likely correct.
    // It is always correct when saying composite though.
    static PRIMES: [u64; 13] = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41];

    let two = Integer::from(2);
    if *n < two {
        return PrimeResult::NotPrimeIsBelowTwo;
    } else if *n == two {
        return PrimeResult::ProbablePrime;
    }

    // Start by doing simple trial division with the primes list
    for a in PRIMES.iter() {
        if n.cmp(*a) != Ordering::Greater {
            assert!(*n == Integer::from(*a));
            return PrimeResult::Prime;
        }
        let rest = n.remainder_digit(*a);

        if rest == 0 {
            return PrimeResult::CompositeFactor(Integer::from(*a));
        }
    }

    let mut n_sub_1 = n.clone();
    n_sub_1.decrement(1);
    let mut n_sub_2 = n_sub_1.clone();
    n_sub_2.decrement(1);
    let s = (&n_sub_1 ^ &n_sub_2).popcnt() - 1;

    let d = n >> s;

    for a in PRIMES.iter() {
        let aa = Integer::from(*a);
        if is_witness(n, &d, s, &aa) {
            return PrimeResult::CompositeWitness(aa);
        }
    }

    if *n < Integer::from_string("3317044064679887385961981").unwrap() {
        PrimeResult::Prime
    } else {
        PrimeResult::ProbablePrime
    }
}


pub fn is_more_prob_prime(n: &Integer) -> PrimeResult {
    // Note: This function is intended to be used on Integers that is_prob_prime() already have returned
    // the ProbablePrime value for.
    // It will test 100 randomly choosen bases to try to find a witness of compositness.

    let mut n_sub_1 = n.clone();
    n_sub_1.decrement(1);
    let mut n_sub_2 = n_sub_1.clone();
    n_sub_2.decrement(1);
    let s = (&n_sub_1 ^ &n_sub_2).popcnt() - 1;

    let d = n >> s;


    let a = Integer::from(43);
    if is_witness(&n, &d, s, &a) {
        return PrimeResult::CompositeWitness(a);
    }

    for _ in 0..100 {
        let a = Integer::from_random_n_bits(10);

        if is_witness(&n, &d, s, &a) {
            return PrimeResult::CompositeWitness(a);
        }
    }
    PrimeResult::ProbablePrime
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_prob_prime() {
//        let n = Integer::from_string("3317044064679887385961981").unwrap();

        for n in 42..60 {
            let n = Integer::from(n);
            match is_prob_prime(&n) {
                PrimeResult::Prime => println!("{} prime", n),
                PrimeResult::CompositeWitness(witness) => println!("{} not prime, we have a witness {}", n, witness),
                _ => ()
            }
        }
    }
}