
use crate::integer::Integer;
use super::is_prob_prime;
use super::PrimeResult;

pub fn gen_prob_prime(bits: usize, attempts: u32) -> Option<(Integer, PrimeResult)> {
    for _ in 0..attempts {
        let n = Integer::from_random_n_bits(bits);

        let pr = is_prob_prime(&n);
        match pr {
            PrimeResult::Prime => return Some((n, PrimeResult::Prime)),
            PrimeResult::ProbablePrime => return Some((n, PrimeResult::ProbablePrime)),
            PrimeResult::NotPrimeIsBelowTwo => (),
            PrimeResult::CompositeWitness(_witness) => (),
            PrimeResult::CompositeFactor(_factor) => (),
        }
    }
    None
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ge_n_prob_prime() {
        for i in 1..=16 {
            let n = gen_prob_prime(i, 1000);
            if let Some((p, pr)) = n {
                match pr {
                    PrimeResult::Prime => println!("{}: Prime({})", i, p),
                    PrimeResult::ProbablePrime => println!("{}: ProbablePrime({})", i, p),
                    _ => (),
                }
            } else {
                println!("{}: FAIL", i);
            }
            println!("");
        }
    }
}
