
use crate::integer::{DoubleDigit, Digit, Integer};


pub fn powmod(n: &Integer, e: &Integer, m: &Integer) -> Integer {
    let mut prod = Integer::from(1);

    let mut n = n.clone();
    let mut e = e.clone();

    n = n.remainder(&m);
    while e.cmp(0) == std::cmp::Ordering::Greater {
        if (&e & 1) == 1 {
            prod = (&prod * &n).remainder(&m);
        }
        e.div_by_2();
        n = (&n * &n).remainder(&m);
    }
    prod
}


pub fn powmod_digit(mut n: Digit, mut e: Digit, m: Digit) -> Digit {
    let mut prod = 1;

    n %= m;
    while e > 0 {
        if (e & 1) == 1 {
            prod = (((prod as DoubleDigit) * (n as DoubleDigit)) % (m as DoubleDigit)) as Digit;
        }
        e >>= 1;
        n = (((n as DoubleDigit) * (n as DoubleDigit)) % (m as DoubleDigit)) as Digit;
    }
    prod
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_powmod() {
        // Testing values with Fermat's little theorem
        // For p prime,and a not divisible by p, then
        // a^(p-1) mod p = 1

        // Prime from https://en.wikipedia.org/wiki/List_of_prime_numbers#The_first_1000_prime_numbers
        let a = Integer::from_string("581").unwrap();
        let pm1 = Integer::from_string("7918").unwrap();
        let p = Integer::from_string("7919").unwrap();

        let res = Integer::from_string("1").unwrap();

        assert_eq!(powmod(&a, &pm1, &p), res);
    }
}

