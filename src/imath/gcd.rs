
use std::cmp::{min, Ordering};
use std::mem::swap;

use crate::integer::Integer;


pub fn gcd(a: &Integer, b: &Integer) -> Integer {
    let mut a = a.clone();
    let mut b = b.clone();

    if b.is_zero() {
        return a;
    }

    let mut rest = a.remainder(&b);
    while !rest.is_zero() {
        a = b;
        b = rest;
        rest = a.remainder(&b);
    }
    b
}


pub fn gcd_binary(a: &Integer, b: &Integer) -> Integer {
    if a.is_zero() {
        return b.clone();
    }
    if b.is_zero() {
        return a.clone();
    }

    let mut a = a.get_number().clone();
    let mut b = b.get_number().clone();

    let a2s = a.trailing_zeros();
    let b2s = b.trailing_zeros();
    a.shift_right(a2s);
    b.shift_right(b2s);


    let min_2s = min(a2s, b2s);

    loop {
        if a.cmp(&b) == Ordering::Greater {
            swap(&mut a, &mut b);
        }

        b.sub(&a);

        if b.is_zero() {
            a.shift_left(min_2s);
            return Integer::from_number(a);
        }

        b.shift_right(b.trailing_zeros());
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_gcd() {
        let a = Integer::from_string("15").unwrap();
        let b = Integer::from_string("21").unwrap();
        let d = gcd(&a, &b);
        
        assert_eq!(d, Integer::from_string("3").unwrap());

        // These are three "big" primes
        let p1 = Integer::from_string("898964570111976099973552681907").unwrap();
        let p2 = Integer::from_string("853980446865340083160260249096827").unwrap();
        let p3 = Integer::from_string("647844822362039375312122608061").unwrap();

        let a = &p1 * &p2;
        let b = &p1 * &p3;
        let d = gcd(&a, &b);
        
        assert_eq!(d, p1);
    }
}
