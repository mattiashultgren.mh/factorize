
use crate::integer::Digit;
use super::powmod_digit;
use super::is_quadratic_residue_digit;


fn popcnt(mut n: Digit) -> Digit {
    // For details see https://en.wikipedia.org/wiki/Hamming_weight#Efficient_implementation: popcnt64d
    let mut count = 0;

    while n > 0 {
        n &= n - 1;
        count += 1;
    }
    count
}


/// Find the sqrt of a number n modulo an odd prime p.
/// Note: Parameter p must be an odd prime number below 2^(BIT_SIZE/2)!
/// Note 2: The sqrt have two solutions but this function only return one of them, say R
///         the second one of them can be found by using the negative of R, i.e. p - R.
pub fn sqrt_mod_p(n: Digit, p: Digit) -> Option<Digit> {
    let n = n % p;
    if (p & 3) == 3 {
        // p = 3 (mod 4)
        let r = powmod_digit(n, (p + 1) / 4, p);
        return if (r * r) % p == n {
            Some(r)
        } else {
            None
        }
    } else {
        // Uses the Tonelli-Shanks algorithm as found at https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm

        //println!("sqrt_mod_p({}, {})", n, p);

        // Step 1: Write p-1 = q * 2^s
        let p_sub_1 = p - 1;
        let p_sub_2 = p - 2;
        let s = popcnt(p_sub_1 ^ p_sub_2) - 1;
        let q = &p_sub_1 >> s;

        //println!("Step 1: q={} s={}", q, s);
        
        // Step 2: Find z as a quadratic non-residue.
        let mut z = 2;
        loop {
            if !is_quadratic_residue_digit(z, p) {
                break;
            }
            z += 1;
        }

        //println!("Step 2: z={}", z);

        // step 3: Setup a bunch of variables
        let mut m = s;
        let mut c = powmod_digit(z, q, p);
        let mut t = powmod_digit(n, q, p);
        let mut r = powmod_digit(n, (q+1) / 2, p);

        //println!("Step 3: m={} c={} t={} r={}", m, c, t, r);

        // Step 4: Loop until t is 0 or 1
        loop {
            //println!("Iteration start");
            if t == 0 {
                //println!(" t == 0");
                return Some(0);
            }
            else if t == 1 {
                //println!(" t == 1");
                return Some(r);
            }

            let mut i = 1;
            let mut tmp_t = t.clone();
            loop {
                if i == m {
                    return None;
                }
                tmp_t = (tmp_t * tmp_t) % p;
                if tmp_t == 1 {
                    break;
                }
                i += 1;
            }
            //println!(" i={}", i);
            let mut tmp_i = &m - &i - 1;
            //println!(" tmp_i={}", tmp_i);
            let mut b = c;
            while tmp_i != 0 {
                b = (b * b) % p;
                tmp_i -= 1;
            }
            //println!(" b={}", b);

            m = i;
            c = (b * b) % p;
            t = (t * c) % p;
            r = (r * b) % p;

            //println!(" m={} c={} t={} r={}", m, c, t, r);
        }
    }
}


/// Computes the sqrt(n) mod (p^e) with the help of the root of the base sqrt.
/// I.e. root = sqrt(n) mod p
pub fn sqrt_mod_p_pow_e(n: Digit, p: Digit, e: u32, root: Digit) -> Digit {
    // Uses the formula from A. Tonelli see https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm#Tonelli's_algorithm_will_work_on_mod_p^k

    let p_pow_em1 = p.pow(e - 1);
    let p_pow_e = p_pow_em1 * p;
    let root_pow = powmod_digit(root, p_pow_em1, p_pow_e);
    let n_pow = powmod_digit(n, (p_pow_e - 2*p_pow_em1 + 1) / 2, p_pow_e);

    (root_pow * n_pow) % p_pow_e
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sqrt_mod_p() {
        // Example from https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm#Example
        let n = 5;
        let p = 41;

        if let Some(r) = sqrt_mod_p(n, p) {
            println!("{}", r);
            let result = r == 28  ||  r == 13;
            assert!(result);
        } else {
            panic!("The test failed to find the sqrt!");
        }
    }


    #[test]
    #[should_panic]
    fn test_sqrt_mod_p_failure() {
        use crate::integer::Integer;

        let n = Integer::from(1239590434); // = 15 (mod 19)
        let p = 19;

        let digit_n = n.remainder_digit(p);
        if let Some(r) = sqrt_mod_p(digit_n, p) {
            let result = r == 28  ||  r == 13;
            assert!(result);
        } else {
            panic!("The test failed to find the sqrt!");
        }
    }


    #[test]
    fn test_prime_powers() {
        use crate::integer::Integer;
        
        let primes: Vec<Digit> = vec![3, 5, 7, 11, 13, 17, 19, 23];
        let exponents: Vec<u32> = vec![2, 3, 4, 5, 6, 7];
        let n = Integer::from_random_n_bits(50);

        for p in primes {
            let digit_n = n.remainder_digit(p);

            if let Some(root) = sqrt_mod_p(digit_n, p) {
                for e in exponents.iter() {
                    let pow_root = sqrt_mod_p_pow_e(digit_n, p, *e, root);

                    if pow_root >= p {
                        println!(">= p, {} {} {} {}", p, pow_root, p.pow(*e) - pow_root, root);
                    }

                    assert_eq!((pow_root * pow_root) % (p.pow(*e)), digit_n,
                               "Failed for n: {}, root: {}, p: {}, e: {}", digit_n, root, p, e);
                }
                println!("Passing for prime {}", p);
            } else {
                println!("n: {} (mod p) = {} is not a quadratic residue of {}", n, digit_n, p);
            }
        }
    }


    #[test]
    fn test_prime_powers_2() {
        let p = 5;
        let n = 519344;

        if let Some(root) = sqrt_mod_p(n, p) {
            println!("p: {} n:{} root:{}",p, n, root);

            let p2 = p*p;

            let root2 = sqrt_mod_p_pow_e(n, p, 2, root);

            println!("p2: {} n:{} root:{}",p2, n, root2);

            assert_eq!((root2*root2) % p2, n % p2);
        } else {
            assert!(false, "Failed to find the sqrt_mod_p");
        }
    }
}