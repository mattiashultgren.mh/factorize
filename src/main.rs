
use std::time::{Duration, Instant};

mod primes;
mod integer;
mod imath;
mod matrix;
mod factorize;

use factorize::factor;

use integer::{Digit, Integer};


fn fermat(m: Digit) -> Integer {
    let mut num = Integer::from(2);
    
    for _ in 0..m {
        num = &num * &num;
    }

    num.increment(1);
    num
}


fn main() {
    let numbers = vec![
        integer::Integer::from_string("493").unwrap(), // ~10 bits
        integer::Integer::from_string("519341").unwrap(), // ~20 bits
        integer::Integer::from_string("619795217").unwrap(), // ~30 bits
        integer::Integer::from_string("384467673613").unwrap(), // ~40 bits
        integer::Integer::from_string("485863540726651").unwrap(), // ~50 bits
        integer::Integer::from_string("556708009790782627").unwrap(), // ~60 bits
        integer::Integer::from_string("578624834532990238661").unwrap(), // ~70 bits
        integer::Integer::from_string("348425397580637765388397").unwrap(),  // ~80 bits
        integer::Integer::from_string("719501770710424592033151703").unwrap(), // ~90 bits
        integer::Integer::from_string("688261849828615226244050458031").unwrap(), // ~100 bits
        integer::Integer::from_string("678962461529919056487948235239607").unwrap(), // ~110 bits
        integer::Integer::from_string("1096307293310212410715681056060014483").unwrap(), // ~120 bits
        integer::Integer::from_string("1085876895991502399853349151080205744051").unwrap(),  // ~130 bits
        integer::Integer::from_string("801317496172946001644900537824938671550911").unwrap(),  // ~140 bits
        integer::Integer::from_string("618732963766846971795886496949197932749170783").unwrap(),  // ~150 bits
        integer::Integer::from_string("729032128007959110589952794774884508594119048187").unwrap(),  // ~160 bits
        integer::Integer::from_string("593139873395036262093662270781704480326206912925065419").unwrap(),  // ~180 bits
        integer::Integer::from_string("1220378729003557943893381216568346239354836839834027110806087").unwrap(),  // ~200 bits
        integer::Integer::from_string("980622076669757112111186651330607757428410419635161598559431873127").unwrap(),  // ~220 bits
        integer::Integer::from_string("1114919804886924471308082281295171047451567756596419189117540723522207421").unwrap(),  // ~240 bits
        integer::Integer::from_string("958807680163669692639147781138079179294021342436101692656423393784867923513177").unwrap(),  // ~260 bits
        /* RSA-100 => 330 bits */ integer::Integer::from_string("1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139").unwrap(),
        /* RSA-1024 */ integer::Integer::from_string("135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563").unwrap(),
        fermat(1),
        fermat(2),
        fermat(3),
        fermat(4),
        fermat(5),
        fermat(6),
        fermat(7),
        fermat(8),
    ];
    
    let mut timings = Vec::new();

    for n in numbers {
        println!("{} ({} bits)", n, n.top_bit_index() + 1);
        let timer = Instant::now();
        let mut rounds = 0;
        while timer.elapsed() < Duration::from_millis(1000) {
            rounds += 1;
            match factor(&n) {
                factorize::FactorResult::CompositeFactor(f, _stats) => println!("Found factor {}", f),
                factorize::FactorResult::Prime => println!("Number is prime!"),
                factorize::FactorResult::ProbablePrime => println!("Number is probably prime"),
                _ => println!("Failed to find a factor!"),
            }
        }
        let elapsed = timer.elapsed() / rounds;
        timings.push((n.top_bit_index() + 1, elapsed));
        println!("\n\n");
    }


    println!("\nTimings:");
    for i in 0..timings.len() {
        println!("{}; {},{:09}", timings[i].0, timings[i].1.as_secs(), timings[i].1.subsec_nanos());
    }
 /*
 
    let p_sizes = vec![20];
    let p_diff = 0;
    for p_size in &p_sizes {
        let (p1, _) = imath::gen_prob_prime(p_size - p_diff, 1000).unwrap();
        let (p2, _) = imath::gen_prob_prime(p_size + p_diff, 1000).unwrap();
        let composite = &p1 * &p2;
        println!("\n\n{} ({} bits) * {} ({} bits) => {} ({} bits)",
                    p1, p1.top_bit_index() + 1,
                    p2, p2.top_bit_index() + 1,
                    composite, composite.top_bit_index() + 1);
        let now = Instant::now();
        if let factorize::FactorResult::CompositeFactor(n, _stats) = factor(&composite) {
            let elapsed = now.elapsed();
            println!("Found factor {} in {:?}", n, elapsed);
        } else {
            println!("Failed to find a factor!");
        }
        println!("\n\n");
    }
    */
}

