# Factorize

My main focus with this project is to learn rust.

Secondary goal is to learn the inner gory details of the quadratic sieve for integer factorization.

Thus, this project will not use too many external libraries as I want to be forced to implement some level of depth in
the hierachies of modules and structures.
